#!/usr/bin/perl

use warnings;
use strict;

use Carp qw(carp confess);
use Data::Dumper;
use File::Copy;
use File::Path qw(make_path);
use Getopt::Long;
use Hash::Util qw(lock_hash);
use IPC::Run3;
use POSIX;
use Proc::PID::File;
use WWW::Telegram::BotAPI;
use YAML qw(DumpFile LoadFile);

die "Already running!"  if Proc::PID::File->running();

my $NOT = 0;
my @CRITICAL_LOG_DEF = (
    "vid_v4lx_start: Failed to open video device"
    ,"motion_loop: Video device fatal error"
    ,qr(Iso frame \d has error)
);
my $SEND_PHOTOS = 1;

my $CONFIG = {
    token => ''
    ,video => {
        size => "320x200"
        ,pre => 'libx264-fast'
        ,ext => 'mp4'
    }
    ,dir_tmp => '/var/tmp' 
    ,motion => {
        dir => '/var/lib/motion' 
        ,archive_file => 1
    },
    ,debug => 2
    ,min_time_send => 300 
    ,syslog => {
        file => '/var/log/syslog' 
        ,critical => \@CRITICAL_LOG_DEF
    }
    ,audio_player => 'mpg123'
    ,file_alarm => ''
    ,min_time_alarm => 300
    ,network => {
        device => 'wlan0,wlan1'
        ,n_packets => 100
        ,min_wait_dump => 60
        ,file_macs => '/var/lib/bruc_macs.yml'
    }
    ,netrogues => {
        # alarm if we spot a rogue for these minutes
        minutes_alarm => 5
        ,times_rogue_neighbour => 50
    }
};

my $FILE_CONFIG = "/etc/bruc.conf";

my ($help, $CLEAN, $DEBUG);
GetOptions(
    config => \$FILE_CONFIG
    ,debug => \$DEBUG
    ,clean => \$CLEAN
    ,help => \$help
) or exit;

if ($help) {
    print("$0 [--clean] [--help] [--config=$FILE_CONFIG\n"
        ."  --clean : Clean old pictures and videos when starting\n"
    );
    exit;
}

my ($me) = $0 =~ m{.*/(.*)};
my $FILE_LOG = "/var/log/$me.log";
my $FILE_LOG_ROGUE = "/var/log/$me.rogue.log";
my $FILE_LOG_TCPDUMP= "/var/log/$me.tcpdump.log";

# rogue detections older than that get cleaned
my $TIME_OLD_ROGUE;

my $VERBOSE = $ENV{TERM};
my $TCPDUMP = `which tcpdump`;
chomp $TCPDUMP;
die "Missing tcpdump"
    if !$TCPDUMP;

clean_old_files() if $CLEAN;

##############################################################
#
# GLOBALS
my ($TELEGRAM, $CHAT_ID);
my $TIME_PHOTO_SENT = 0;
my $TIME_VIDEO_SENT = 0;
my $TIME_TEXT_SENT = 0;

my $TIME_TCPDUMP_LAST = 0;
my $TIME_ALARM_LAST = 0;

my %VIEWED;

my $DEVICE; # net device
my $MAC;

# TODO see if we should store in disk
my %NETWORK;
############################################################

sub clean_old_files {
#    $SIG{CHLD} = 'IGNORE';
    my $pid = fork();
    if (!$pid) {
        do_clean_old_files();
        exit;
    }
    warn "going to normal bruc";
}

sub do_clean_old_files {
    my $time0 = time;
    my $count = 0;
    warn $ENV{TERM};
    opendir my $ls,$CONFIG->{motion}->{dir} or die "$! $CONFIG->{motion}->{dir}";
    while (my $file = readdir $ls) {
        $file = $CONFIG->{motion}->{dir}."/$file";
        warn $file;
        if ( !-f $file ) {
            next;
        }
        warn "unlinking $file";
        unlink $file or warn "$! $file";
        warn "done" if $DEBUG;
        $count++;
        my $time2 = time() - $time0;
        if ($ENV{TERM} && $time2 > 10) {
            print localtime(time)." Cleaning $count files in $CONFIG->{motion}->{dir}\n";
            $time0 = time;
        }
    }
    closedir $ls;
}

sub connect_tg {
    warn ''.localtime(time)." connecting to telegram"   if $DEBUG;
    $TELEGRAM = undef;

    $TELEGRAM = WWW::Telegram::BotAPI->new (
        token => $CONFIG->{token}
    )or die "I can't connect to Telegram";
}

sub init_telegram {
    return if $TELEGRAM && $CHAT_ID;
    for my $cont ( 1 .. 10 ) {
        eval {
            connect_tg();
            if ( !$TELEGRAM ) {
                warn $@ if $@;
                sleep $cont;
                next;
            }
            my $out;
            $out = $TELEGRAM->api_request ('getUpdates');
            $CHAT_ID = $out->{result}->[0]->{message}->{chat}->{id}
                or die "I can't find the chat id in ".Dumper($out);
            return if $CHAT_ID;
        };
        return if $TELEGRAM && $CHAT_ID;
        warn "tg=".($TELEGRAM or '<UNDEF>')." , chat_id=".($CHAT_ID or '<UNDEF>');
        warn "[$cont] no connection to tg ".($@ or '');
        $TELEGRAM = undef;
        sleep $cont;
    }
}

sub send_text {
    my $text = shift;
    
    if ( time - $TIME_TEXT_SENT < $CONFIG->{min_time_send} ) {
        warn "text sent at $TIME_TEXT_SENT";
        return;
    }
    init_telegram();
    eval {
        $TELEGRAM->sendMessage ({
            chat_id => $CHAT_ID,
            text    => $text
        });
    };
    if ($@) {
        $TELEGRAM = undef;
        return;
    }
    $TIME_TEXT_SENT = time;
}

sub resize_video {
    my $file = shift;

    my ($file_out) = $file =~ m{.*/(.*)\.\w+$};
    $file_out = "$CONFIG->{dir_tmp}/$file_out.$CONFIG->{video}->{ext}";

    if ($file_out eq $file ) {
        $file_out = "$CONFIG->{dir_tmp}/$file_out.tmp.$CONFIG->{video}->{ext}";

    }
    return $file_out if -e $file_out;

    my @cmd = ("avconv","-i",$file,'-pre',$CONFIG->{video}->{pre}
                ,"-s",$CONFIG->{video}->{size}
                ,'-strict','experimental', $file_out
    );
    my ($in, $out, $err);
    warn join(" ",@cmd) if $DEBUG;
    run3 (\@cmd, \$in, \$out, \$err);
    die $err if $?;
    return $file_out;
}

sub finished_file {
    my $file = shift;
    my @stat = stat($file) or do {
        warn "I can't stat $file";
        return;
    };
    warn $file. " ".(time - $stat[9])." ".localtime($stat[9])."\n"
        if $DEBUG;
    return time - $stat[9] > 1;
}

sub unmark_viewed {
    my $file = shift;
    $file =~ s{.*/}{};
    delete $VIEWED{$file};
    warn "unmarking $file viewed\n" if $DEBUG;
}

sub send_video {
    my $file = shift;

    if (!finished_file($file)) {
        unmark_viewed($file);
        return;
    } 

    my $file_small = resize_video($file);
    warn "sending $file_small"  if $DEBUG;
    return if $NOT;
    init_telegram();
    return if !$TELEGRAM;
    eval {
    $TELEGRAM->api_request('sendVideo'
        ,{
            chat_id => $CHAT_ID
            ,video => {
                file => $file_small
            }
            ,caption => "Video at ".localtime(time)
        }
    );
    };
    if ($@) {
        $TELEGRAM = undef;
        warn $@;
        return;
    }
    warn "done.\n"  if $DEBUG;
    $TIME_VIDEO_SENT = time;
}

sub send_photo {
    my $file = shift;

    return if !$SEND_PHOTOS;
    warn "sending photo $file"  if $DEBUG;

    return if $NOT;
    init_telegram();
    return if !$TELEGRAM;

    my @stat = stat($file);
    eval {
        $TELEGRAM->sendPhoto ({
            chat_id => $CHAT_ID,
            ,photo   => {
                file => $file
            }
            ,caption => ''.localtime($stat[9])
        });
    };
    if ($@) {
        warn $@;
        $TELEGRAM = undef;
    }
    warn "done.\n"  if $DEBUG;
}

sub is_photo {
    my $file = shift;
    return 1 if $file =~ m{(jpg|jpeg|png|gif)$};
    return;
}

sub is_video {
    my $file = shift;
    return 1 if $file =~ m{(mp4|mov|avi|mkv)$};
    return;
}

sub send_file {
    my $file = shift;

    if (is_photo($file)) {
        return if !$SEND_PHOTOS;
        if ( time - $TIME_PHOTO_SENT < $CONFIG->{min_time_send} ) {
            warn " .. skiping .. Sent photo ".(time - $TIME_PHOTO_SENT)
                ."s ago"    if $CONFIG->{debug}>1;
            return;
        }
        send_photo($file);
        $TIME_PHOTO_SENT = time;
    } elsif (is_video($file)) {
        if ( time - $TIME_VIDEO_SENT < $CONFIG->{min_time_send} ) {
            warn " .. skiping .. Sent video ".(time - $TIME_VIDEO_SENT)
                ."s ago"    if $CONFIG->{debug}>1;
            return;
        }
        send_video($file);
    } else {
        carp "I don't know what kind of file is this $file";
    }
}

sub mark_all_viewed {


    opendir( my $ls, $CONFIG->{motion}->{dir}) or die "$! $CONFIG->{motion}->{dir}";
    while (my $file = readdir $ls) {
        next if $file =~ /^\./;
        next if -d "$CONFIG->{motion}->{dir}/$file";
        my @stat = stat("$CONFIG->{motion}->{dir}/$file");
        next if !$CLEAN && $stat[9]
                && time - $stat[9] < $CONFIG->{min_time_send}
                && is_photo($file);
        $VIEWED{$file}++;
        archive_file($file);
    }
    closedir $ls;
}

sub archive_file {
    my $file = shift;

    $file = $CONFIG->{motion}->{dir}."/".$file
        if $file !~ m{^/};

    my @stat = stat($file) or do {
        warn "I can't stat $file";
        return;
    };
    my $mtime = $stat[9];

    my @time = localtime($mtime);
    $time[4]++;
    $time[5]+=1900;

    for (0 .. 4 ) {
        $time[$_] = "0".$time[$_]   if length($time[$_]) < 2;
    }
    my $path = join("/",reverse @time[0..5]);

    my $dst = $CONFIG->{motion}->{dir}."/".$path;
    make_path($dst)
        if ! -e $dst;

    print "$file -> $dst\n" if $DEBUG;
    copy($file, $dst) or do {
        confess "$! $file -> $dst";
        return;
    };
    unlink($file) or warn "$! $file";
}

sub watch_motion {
    print localtime(time)." watching $CONFIG->{motion}->{dir}\n"  
        if $CONFIG->{debug}
            && int(time/60) == time/60; 

    my $alarm = 0;

    opendir( my $ls, $CONFIG->{motion}->{dir}) or die "$! $CONFIG->{motion}->{dir}";
    while (my $file = readdir $ls) {
        next if $file =~ /^\./;
        next if !-f "$CONFIG->{motion}->{dir}/$file";
        if (!$VIEWED{$file}++ ) {
        
            print " --> $file\n"    if $CONFIG->{debug};
            send_file("$CONFIG->{motion}->{dir}/$file");
            $alarm++;
        }
        archive_file($file) if $CONFIG->{motion}->{archive_file}
            && $VIEWED{$file};
    }
    closedir $ls;
    play_alarm("motion detected") if $alarm;
    return $alarm;
}

sub handle_critical {
    warn "CRITICAL $_[0]";
    print `reboot`;
}

sub check_video_device {
    open my $tail,"-|","tail -10 $CONFIG->{syslog}->{file}" 
        or die "$! tail -f $CONFIG->{syslog}->{file}";
    while (my $line = <$tail>) {
        for (@{$CONFIG->{syslog}->{critical}}) {
            handle_critical($line)
                if  $line =~ /$_/;
        }
    }
    close $tail;
}

sub load_config {
    if (! -e $FILE_CONFIG) {
        DumpFile($FILE_CONFIG,$CONFIG) or die "$! $FILE_CONFIG";
    } else {
        $CONFIG = LoadFile($FILE_CONFIG);
    }
    die "Missing token" if !$CONFIG->{token};

    $CONFIG->{debug} = $DEBUG   if $DEBUG;
    lock_hash(%$CONFIG);
}

sub init {
    load_config();

    $TIME_OLD_ROGUE= ($CONFIG->{netrogues}->{minutes_alarm}+1) * 60;
}


sub set_dev_down {
    my $dev = shift;

    my @cmd = ('ifconfig', $dev, 'down');
    my ($in, $out, $err);
    warn join(" ",@cmd)."\n";
    run3(\@cmd, \$in, \$out, \$err);
    die join(" ",@cmd)." $? ".$err if $?;
    sleep 1;
}

sub set_dev_up{
    my $dev = shift;

    my @cmd = ('ifconfig', $dev, 'up');
    my ($in, $out, $err);
    warn join(" ",@cmd)."\n";
    run3(\@cmd, \$in, \$out, \$err);
    die join(" ",@cmd)." $? ".$err if $?;
    sleep 1;
}


sub set_dev_promisc {
    my $dev = shift;

    my @cmd = ( 'iwconfig', $dev, 'mode', 'monitor');
    my ($in, $out, $err);
    warn join(" ",@cmd)."\n";
    run3(\@cmd, \$in, \$out, \$err);
    die join(" ",@cmd)." $? ".$err if $?;
    sleep 1;
}

sub watch_wifi {

    init_dev();
    return if !$DEVICE;

    my @cmd = ($TCPDUMP,'-i',$DEVICE,'-n','-e'
        ,'-c', $CONFIG->{network}->{n_packets});

    my $found;

    my $time0 = time;
    for (;;) {
        my ($in, $out, $err);
        run3(\@cmd, \$in, \$out, \$err);

        for my $line (split /\n/,$out) {

            print_log_tcpdump($line);
            my ($mac, $bssid, $beacon, $probe_req ) = extract_data($line);
            next if !$mac;

            set_network($beacon, $bssid) if $beacon && $bssid;
            next if $beacon;

            $found->{$mac}->{count}++;
            if ($bssid) {
                my $network = get_network($bssid);
                $found->{$mac}->{bssid}->{$bssid}++;
                $found->{$mac}->{network}->{$network}++     if $network;
             }
             $found->{$mac}->{beacon}->{$beacon}++     if $beacon;
             $found->{$mac}->{probe_req}->{$probe_req}++   if defined $probe_req;
        }
        # capture for at least some seconds, TODO configure
        last if time - $time0 > 30;
    }
    return search_rogue($found);

}

sub extract_data {
    my $line = shift;
    my ($mac) = $line =~ /.*Broadcast.*SA:([0-9A-Fa-f:]+).*Probe Request/i;
    ($mac) = $line =~ /.*ff:ff:ff:ff:.*SA:([0-9A-Fa-f:]+).*Probe Request/i
            if !$mac;

    my ($bssid) = $line =~ /BSSID:(.*?) /;
    $bssid = '' if !defined $bssid;
    $bssid = '' if $bssid && $bssid =~ /broadcast/i;

    my ($beacon) = $line =~ /Beacon \((.*?)\)/;
    my ($probe_req) = $line =~ /Probe Request \((.*?)\)/i;
    $probe_req = lc($probe_req) if $probe_req;

    return ($mac, $bssid, $beacon, $probe_req);
}

sub get_network {
    my $mac = shift or return;
    $mac =~ s/:..:..$//;
    my $net = $NETWORK{$mac};
    return lc($net) if $net;
}

sub is_daddy {
# TODO
}

sub ping_daddy {
# TODO
}

sub is_neighbour {
    my $mac = shift;
    return $MAC->{$mac}->{neighbour}
        if $MAC->{$mac} && $MAC->{$mac}->{neighbour};
}

sub store_known {
    DumpFile($CONFIG->{network}->{file_macs}, $MAC) or warn "$! FILE_MAC";
}

sub set_neighbour {
    my $mac = shift;
    my $net = shift;
    confess "missing net for set_neighbour" if !$net;
    return if $MAC->{$mac}->{neighbour}
            && $MAC->{$mac}->{neighbour} !~ /:/;
    return if $net =~ /:/;
    print "setting neighbour of $mac to $net\n" if $DEBUG;
    $MAC->{$mac}->{neighbour}=$net;
    store_known();
}

sub set_rogue {
    my $mac = shift;
    push @{$MAC->{$mac}->{rogue}},(time);
}

sub rogue_neighbour {
    my $mac = shift or confess "Missing MAC";
    my $times = scalar(@{$MAC->{$mac}->{old_rogue}})
        if $MAC->{$mac}->{old_rogue};
    print "  $mac spotted $times times as old rogue.\n"
        if $times;

    # we clean it on purpose after counting we give it one chance
    # if just passes by
#    clean_old_rogue_neighbour($mac);

    return 0 if !$times || $times<$CONFIG->{netrogues}->{times_rogue_neighbour};
    return 1;
}

sub spotted_for_minutes {
    my $mac = shift;
    warn "*Searching spotted for $mac \n";
    my $count = 0;

    my @old_rogue;
    eval {@old_rogue = @{$MAC->{$mac}->{old_rogue}}};
    my @rogue;

    for my $time_rogue (@{$MAC->{$mac}->{rogue}}) {
        my $minutes = (time - $time_rogue ) / 60;
        if ($minutes <= $CONFIG->{netrogues}->{minutes_alarm} ) {
            print "$mac spotted $minutes minutes ago\n" if $DEBUG;
            push @rogue,($time_rogue);
            $count++;
        } else {
            push @old_rogue,($time_rogue);
        }
    }
    if (scalar @old_rogue) {
        $MAC->{$mac}->{old_rogue} = \@old_rogue;
        $MAC->{$mac}->{rogue} = \@rogue;
    }
    print "* $count spotted recently\n" if $DEBUG;
    return $count>= $CONFIG->{netrogues}->{minutes_alarm};
}

sub search_rogue {
    my $found = shift;

    return if !scalar keys %$found;
    my @rogue;
    print_log("SEARCHING for rogues in ".scalar(keys %$found)." MACs found\n");

    MAC:
    for my $mac ( sort keys %$found) {
        next if is_daddy($mac);
        if (my $net = is_neighbour($mac)) {
            print_log( "  $mac neighbour ($net)\n");
            next;
        }

        my $count = $found->{$mac}->{count};
        for my $type (keys %{$found->{$mac}}) {
            next if !ref $found->{$mac}->{$type};
            for my $net (keys %{$found->{$mac}->{$type}}) {
                next if !$net || $net =~ /^[f:]+$/;
                my $req = $found->{$mac}->{$type}->{$net};
                if ($req >= $count * 0.5 ) {
                    print_log("NEIGHBOUR $mac $req / $count in $net\n");
                    set_neighbour($mac, $net) if $req >= $count*0.8;
                    next MAC;
                }
            }
        }
        set_rogue($mac);
        next if rogue_neighbour($mac);
        next if !spotted_for_minutes($mac);

        my $dump_mac = $MAC->{$mac};
        eval {$dump_mac->{n_old_rogue} = scalar @{$dump_mac->{old_rogue}}};
	my $old_rogue = $dump_mac->{old_rogue}	if $dump_mac->{old_rogue};
        delete $dump_mac->{old_rogue};

        my $text = "***ROGUE*** $mac at home ".Dumper($found->{$mac})
            ."\n".Dumper($dump_mac)
            ."\n";

	    $dump_mac->{old_rogue} = $old_rogue	if $old_rogue;

        send_text($text)    if day_period();
        print_log($text);
        print_log_rogue($text);
        push @rogue ,($mac);
        play_alarm("$mac rogue detected") if day_period();
    }
    if ( @rogue ) {
        print_log( scalar(@rogue)
            ." rogues at home.\n");
        ping_daddy() && return;
    }
    return scalar @rogue;

}

sub day_period  {
    my @now = localtime(time);
    return $now[2]>7 && $now[2]<=23;
}

sub print_log_rogue {
    open my $log,'>>',$FILE_LOG_ROGUE or die "$! $FILE_LOG_ROGUE";
    print $log join('',map { localtime(time).$_ } @_);
    close $log;
}
sub print_log {
    open my $log,'>>',$FILE_LOG or die "$! $FILE_LOG";
    print $log join('', map { localtime(time).$_ } @_);
    print join('',@_) if $VERBOSE;
    close $log;
}

sub print_log_tcpdump {
    open my $log,'>>',$FILE_LOG_TCPDUMP or die "$! $FILE_LOG_TCPDUMP";
    print $log join("\n", map { localtime(time).$_ } @_)."\n";
    close $log;
}

sub load_macs {
    if (!-e $CONFIG->{network}->{file_macs}) {
        $MAC = {};
        return;
    }
    $MAC = LoadFile($CONFIG->{network}->{file_macs}) 
        or die "$! $CONFIG->{network}->{file_macs}";
}

sub init_dev {
    return if $DEVICE;
    for my $dev (split /,/,$CONFIG->{network}->{device}) {
        warn "trying $dev";
        eval {
            set_dev_down($dev);
            set_dev_promisc($dev);
            set_dev_up($dev);
        };
        next if $@;
        warn $@ if $@;
        $DEVICE = $dev;
    }
}

sub play_alarm {
    if (!$CONFIG->{file_alarm}) {
        warn "WARNING: I can't play the alarm, set a file_alarm"
            ," in the config file\n";
        return;
    }
    if (! -e $CONFIG->{file_alarm}) {
        warn "ERROR: Missing file_alarm ".$CONFIG->{file_alarm};
        return;
    }

    return if time - $TIME_ALARM_LAST < $CONFIG->{min_time_alarm};

    send_text("play alarm");

    my @cmd = ($CONFIG->{audio_player}, $CONFIG->{file_alarm});

    warn @cmd;

    my ($in, $out, $err);
    run3(\@cmd,\$in, \$out, \$err);

    $TIME_ALARM_LAST = time;
}

#########################################################
init();

load_macs();
mark_all_viewed();
for (;; ) {
    watch_motion();
    watch_wifi();

    check_video_device();
    sleep 1;
}
